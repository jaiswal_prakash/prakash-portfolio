const express = require('express');// get the express
const app = express();

const port =process.env.PORT || 300


const path = require('path'); // for showing the html 


app.use(express.static(path.join(__dirname, 'views')));

// ----------- defining port number------------
app.listen(4000, () => {
    console.log("listening to the port 4000")
});